import React from 'react';
import './TasksPerDate.css';

const DateTasks = ({ date, tasks }) => {
  return (
    <div className="big-rectangle">
      <h3>Statistic</h3>
      <p>Date: {date}</p>
      <p>Tasks:</p>
      <ul>
        {tasks.map((task, index) => (
          <li key={index}>{task.taskDetails}</li>
        ))}
      </ul>
    </div>
  );
};

export default DateTasks;