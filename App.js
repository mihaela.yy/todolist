import React from 'react'
import { useEffect, useState} from 'react'
import './App.css'
import Form from './Components/Form'
import Todo from './Components/Todo'
import BigRectangleButton from './Components/ShowStatsButton'
import Calendar from 'react-calendar';
import 'react-datepicker/dist/react-datepicker.css';

let App = () => {
    let [ToDoArray, setToDoArray] = useState([]);
    let [In,setIn]=useState(-1);
    let [Str,setStr]=useState('');
    let [taskCount, setCount] = React.useState(0);
    let [checkedCount, setCheckedCount] = React.useState(0);
    let [selectedDate, setSelectedDate] = useState(new Date());
    let [tasksCountForDate, setTasksCountForDate] = useState(0);
    let [showTasksForDate, setShowTasksForDate] = useState(false);
    let [selectedUser, setSelectedUsers] = useState(Array(ToDoArray.length).fill('User 1'));


    let CollectInput = (Value) => 
    {
        if(Str!=='')
        {
            let Temp=[...ToDoArray];
            Temp[In]=Value;
            localStorage.setItem("Todo",JSON.stringify(Temp));
            setToDoArray(Temp);

            setStr('');
            setIn(-1);
        }
        else
        {
            let Temp=[Value,...ToDoArray];
            localStorage.setItem("Todo",JSON.stringify(Temp));
            setToDoArray(Temp);
        }
        setCount((prevCount) => prevCount + 1);

        let newCheckbox = document.getElementById(`todoCheckbox_${ToDoArray.length}`);
        if (newCheckbox && newCheckbox.checked) 
        {
            setCheckedCount((prevCount) => prevCount + 1);
        }
    }

    const countTasksWithDate = (tasks, selectedDate) => {
        return tasks.filter(task => task.date && task.date.toDateString() === selectedDate.toDateString()).length;
      };
    
      const handleShowTasksForDate = () => {
        const count = countTasksWithDate(ToDoArray, selectedDate);
        setTasksCountForDate(count);
        setShowTasksForDate(true);
      }
    
      const handleCloseTasksForDate = () => {
        setShowTasksForDate(false);
      }

    let Delete=(Ind)=>
    {
        let newArr=[...ToDoArray];
        newArr.splice(Ind,1);
        localStorage.setItem("Todo",JSON.stringify(newArr));
        setToDoArray(newArr);
        
        setCount((prevCount) => prevCount - 1);

        let newCheckbox = document.getElementById(`todoCheckbox_${Ind}`);
        if (newCheckbox && newCheckbox.checked) 
        {
            setCheckedCount((prevCount) => prevCount);
        }
    }
    
    let EditText=(Ind,Elem)=>
    {
        setIn(Ind);
        setStr(Elem);    
        setCount((prevCount) => prevCount - 1);
    };

    let UpdateCheckedCount = (index, isChecked) => {
        if (isChecked) {
          setCheckedCount((prevCount) => prevCount + 1);
        } else {
          setCheckedCount((prevCount) => prevCount - 1);
        }
    };

    useEffect(()=>
    {
        let Ar=JSON.parse(localStorage.getItem("Todo"));
        if(Ar)
        setToDoArray(Ar);
    },[]);

      return (
    <div className='App'>
      <div className="App-Content">
        <BigRectangleButton onButtonClick={handleShowTasksForDate} taskCount={taskCount} checkedCount={checkedCount} />
        <Calendar onChange={setSelectedDate} value={selectedDate} />
        {showTasksForDate && (
          <div>
            <h2>Tasks for {selectedDate && selectedDate.toDateString()}:</h2>
            {tasksCountForDate > 0 ?
              <p>{tasksCountForDate} tasks for this date.</p>
              : <p>No tasks for this date.</p>}
            <button onClick={handleCloseTasksForDate}>Close</button>
          </div>
        )}
        <Form Take={CollectInput} Icon={In === -1 ? "fa-solid fa-plus" : "fa-solid fa-marker"} String={Str === '' ? '' : Str} />

        {In !== -1 && (
        <div>
          <label htmlFor="userDropdown">Select User:</label>
          <select
            id="userDropdown"
            onChange={(e) => {
              const selectedUser = e.target.value;
              const newSelectedUsers = [...selectedUser];
              newSelectedUsers[In] = selectedUser;
              setSelectedUsers(newSelectedUsers);
            }}
            value={selectedUser[In]}
          >
            {Array.from({ length: 10 }, (_, index) => (
              <option key={index} value={`User ${index + 1}`}>{`User ${index + 1}`}</option>
            ))}
          </select>
        </div>
      )}
        
        {ToDoArray.length === 0 ? <div className="alert alert-success mt-2" role="alert">
          No To Do Available Yet !!
        </div> :
          <Todo GetTodos={ToDoArray} Del={Delete} Edit={EditText} UpdateCheckedCount={UpdateCheckedCount} />}
      </div>
    </div>
  )
}

export default App;