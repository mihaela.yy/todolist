import React, { useState } from 'react';
import ReactDatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';
import './AddDetails.css';

const EditDetailsButton = ({ Ind, SetDetails, selectedUser }) => {
    const [showDetails, setShowDetails] = useState(false);
    const [selectedDate, setSelectedDate] = useState(null);
    const [editedUser, setEditedUser] = useState(selectedUser);
  
    const handleEditDetails = () => {
      setShowDetails(!showDetails);
    };
  
    const handleSaveDetails = () => {
      const detailsInput = document.getElementById(`detailsInput_${Ind}`);
      const details = detailsInput.value;
  
      SetDetails(Ind, { taskDetails: details, taskDate: selectedDate, taskUser: editedUser });
  
      setShowDetails(false);
    };
  
    return (
      <div className='edit-details-button'>
        <button onClick={handleEditDetails}>Edit Details</button>
        {showDetails && (
          <div className='details-input'>
            <input type='text' id={`detailsInput_${Ind}`} placeholder='Enter details...' />
            <ReactDatePicker
              selected={selectedDate}
              onChange={(date) => setSelectedDate(date)}
              dateFormat="MMMM d, yyyy"
            />
            <label htmlFor={`userDropdown_${Ind}`}>Select User:</label>
            <select
              id={`userDropdown_${Ind}`}
              onChange={(e) => setEditedUser(e.target.value)}
              value={editedUser}
            >
              {Array.from({ length: 10 }, (_, index) => (
                <option key={index} value={`User ${index + 1}`}>{`User ${index + 1}`}</option>
              ))}
            </select>
            <button onClick={handleSaveDetails}>Save Details</button>
          </div>
        )}
      </div>
    );
  };
  
  export default EditDetailsButton;