import React, { useState } from 'react';
import './Todo.css';
import EditDetailsButton from './AddDetails';

const Todo = (Props) => {
  const [isCheckedArray, setIsCheckedArray] = useState(new Array(Props.GetTodos.length).fill(false));

  const [taskDetails, setTaskDetails] = useState([]);

  const setDetails = (index, details) => {
    const updatedDetails = [...taskDetails];
    updatedDetails[index] = details;
    setTaskDetails(updatedDetails);
  };

  const handleDelete = (index) => {
    const isChecked = isCheckedArray[index];

    if (isChecked) {
      Props.UpdateCheckedCount(index, false);
    }

    const newArray = [...isCheckedArray];
    newArray.splice(index, 1);
    setIsCheckedArray(newArray);
    Props.Del(index);
  };

  const handleCheckboxChange = (index) => {
    const newArray = [...isCheckedArray];
    newArray[index] = !isCheckedArray[index];
    setIsCheckedArray(newArray);
    Props.UpdateCheckedCount(index, newArray[index]);
  };

  return (
    <div className='Todo'>
      {
      Props.GetTodos.map((Elem, Ind) => (
        <div className="All-Todo" key={Ind}>
          <div className="group">
            <input type="checkbox" id={`todoCheckbox_${Ind}`} checked={isCheckedArray[Ind]} onChange={() => handleCheckboxChange(Ind)} />
            <label htmlFor={`todoCheckbox_${Ind}`}></label>
          </div>

          <p>{Elem}</p>
          <p>{Elem.value}</p>
          <p>Date: {Elem.date && Elem.date.toDateString()}</p>
          {taskDetails[Ind] && (
            <div className="task-details">
              <p>{taskDetails[Ind].taskDetails}</p>
              <p>{taskDetails[Ind].taskDate && taskDetails[Ind].taskDate.toDateString()}</p>
              <p>User: {Elem.taskUser && Elem.taskUser.userName}</p>
            </div>
          )}
          <i className="fa-solid fa-marker E" onClick={() => Props.Edit(Ind, Elem)}></i>
          <i className="fa-solid fa-circle-xmark X" onClick={() => handleDelete(Ind)}>
          </i><EditDetailsButton Ind={Ind} SetDetails={setDetails} selectedUser={Elem.taskUser} />
         
        </div>
      ))}
    </div>
  );
};

export default Todo;