import React, { useState } from 'react';
import './ShowStats.css';

const BigRectangleButton = ({onButtonClick, taskCount, checkedCount}) => {
    const [showRectangle, setShowRectangle] = useState(false);

    const handleButtonClick = () => {
        setShowRectangle(!showRectangle);
        onButtonClick();
    };

    return (
        <div className="big-rectangle-button">
            <button className='button' onClick={handleButtonClick}>{showRectangle ? 'Hide Statistic' : 'Show Statistic'}</button>
            {showRectangle && (
                <div className="big-rectangle">
                    <h3>Statistic</h3>
                    <p className="count">Total task count: {taskCount}</p>
                    <p className="checked-count">Finished tasks: {checkedCount}/{taskCount}</p>
                    <p className="not-checked-count">Unfinished tasks: {taskCount-checkedCount}/{taskCount}</p>
                </div>
            )}
        </div>
    );
}

export default BigRectangleButton;