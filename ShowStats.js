import React from 'react';
import BigRectangleButton from './BigRectangleButton';

const ShowStats =(Props)=>
{
    let handleButtonClick = () => {
        console.log('Button inside App.js clicked');
    };
    useEffect(() => {
        setText(Props.String)
    }, [Props.String])
    
    return(
        <div className="Stats">
            <BigRectangleButton onButtonClick={handleButtonClick} />
        </div>
    )
}

export default ShowStats;